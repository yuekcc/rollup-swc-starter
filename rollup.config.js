import path from 'path';

import { defineConfig } from 'rollup';
import commonjs from '@rollup/plugin-commonjs';
import css from 'rollup-plugin-css-only';
import nodeResolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import { swc, defineRollupSwcOption } from 'rollup-plugin-swc3';
import vue from 'rollup-plugin-vue';
import alias from '@rollup/plugin-alias';

const DEBUG = process.env.NODE_ENV === 'development';
console.log('DEBUG =', DEBUG);

function swcConfig() {
  const jscMinifyConfig = {
    compress: {
      unused: true,
      drop_console: true,
      drop_debugger: true,
      dead_code: true,
    },
    mangle: {
      keepClassNames: true,
      keepFnNames: false,
    },
  };

  return defineRollupSwcOption({
    include: ['src'],
    jsc: {
      target: 'es2020',
      minify: !DEBUG ? jscMinifyConfig : {},
      parser: {
        syntax: 'ecmascript',
        dynamicImport: true,
      },
    },
    minify: !DEBUG,
    sourceMaps: DEBUG,
  });
}

export default defineConfig({
  input: 'src/index.js',
  output: {
    dir: 'dist',
    format: 'es',
    sourcemap: DEBUG,
    manualChunks(id) {
      if (id.includes('node_modules')) {
        return 'vendor';
      }
    },
  },
  plugins: [
    alias({
      entries: [
        {
          find: '@',
          replacement: path.resolve(__dirname, 'src'),
        },
      ],
    }),
    nodeResolve(),
    commonjs({ transformMixedEsModules: true }),
    vue({
      css: true,
      style: { trim: true },
      exposeFilename: false,
    }),
    css({ output: 'bundle.css' }),
    swc(swcConfig()),
    replace({
      preventAssignment: true,
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production'),
    }),
  ],
});
