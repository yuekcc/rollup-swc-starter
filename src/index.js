import Vue from 'vue/dist/vue.runtime.esm';

!(async () => {
  const App = await import('@/app.vue');
  const Entry = Vue.extend(App.default);
  return new Entry({
    el: '#app',
  });
})();
